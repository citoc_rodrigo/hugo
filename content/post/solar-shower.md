---
subtitle: "Are solar showers really worth it?"
tags: [solar, cold-weather]
draft: false
---

In the portable shower market or the camping shower markets, solar is one of the most sold features. But does it really make a difference?

## What is a solar shower
Although, the name might make you think otherwise a solar shower is not a shower with solar panels. It's simply a shower which has been designed to warm up when put under the sun for a long time.

Typically, the *design* employed by [most manufacturers](https://amzn.to/3xLi0m7) is to color the water container with an opaque color. The point of that color is to have light come in but not be able to get out. This has a sort of greenhouse effect which heats up the water.
With this kind of system, the water in your shower should take about 3 hours in direct sunlight to get warm. That's a long time!

The question is whether showers which don't have opaque colors also get warm under direct sunlight. Although, most of the time when you have direct sunlight you don't want a warm shower!

Here is a short video of a completely transparent solar shower setup:

{{< youtube hHW_tCEMxeE >}}

If you think we should do a side by side comparaison of a black colored shower vs. a transparent shower drop me a quick email : camachorod+pomo@gmail.com
