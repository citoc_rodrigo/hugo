# Where has POMO been? 

Since POMO was launched in November 2020, we have sold just over 2500 units! We are super happy to be helping so many people around the USA. On this journey we have noticed that the uses for POMO are quite diverse. Although they are broad they often fall in three categories: surfing, camping/mountainbiking and fishing. 

We have some great data to show! Below we see a small circle for each purchase of POMO (locations are rounded to ZIP codes to protect personal data). Take a look! 

![POMO sales accros the USA](https://i.ibb.co/TcpxHjg/whole-usa.png)

## Surfing 

We see some big surfing cities ordering a lot of POMO units! Places like : Hawaii, San Diego, San Francisco. I won't bore you with too many images but the most exciting for me was to see so many units delivered to the middle of the pacific ocean! 

![POMO sales in Hawaii](https://i.ibb.co/2Z9bk0z/hawaii-pomo.png) 

It seems that POMO is super useful for Surfing - getting your equipment clean after a session is essential to protect it but also to protect your car from salt and sand! 

## Camping and Mountain biking

I put both of the activities in the same category because they tend to happen close to each other. I decided to use Washington State to show since I lived in Vancouver for many years and know that there is a lot of great camping and mountain biking spots in Washington State! 

![POMO sales near Seattle](https://i.ibb.co/F7pZCv8/Seattle-pomo.png)

I have emailed a few of our customers who have attested to the fact that POMO is a great portable spray for their mountainbike. I love downhill mountainbiking myself and know how important it is to keep your bike clean from mud between runs! That's why [Whistler](https://www.whistlerblackcomb.com/explore-the-resort/activities-and-events/whistler-mountain-bike-park/whistler-mountain-bike-park.aspx) always has great cleaning stations for bikes at the bottom of the mountain. 

When it comes to camping - I think it's best for dishwashing! It's the most dreaded activity when camping in the woods and POMO makes it easy since it has great pressure. Other than that, maybe for keeping your dog clean :). 

## Fishing 

The [first video](https://www.instagram.com/p/CJWbvA0njbE/) of POMO ever was of someone using it to keep their boat clean! We clearly see that a lot of Floridians are following on MonsterMike and Brian the CEO's footsteps. We have sold a lot of units in and near Florida. 

![POMO sales near Seattle](https://i.ibb.co/yhHKT2b/florida-pomo.png)

The pressure built by POMO is great to keep the deck of your boat cleam but also your fishing lines! Salt can really ruin great fhising equipment so sometimes it's good to fill POMO with fresh water and clean off the deck and equipment when you get back from a fishing session. 

What do you use POMO for? Send us an email with some pictures to camachorod+pomo@gmail.com ; we might share them in one of our next data-dive posts! 
