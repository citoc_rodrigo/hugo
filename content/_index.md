+++
aliases = [
    "/bottleqr"
]
+++




![Image of POMO](https://i.ibb.co/ft9zF9T/pomo-smallest.png)

## Useful links
- [Buy POMO at a discount (54.99 USD, free delivery)](https://buy.stripe.com/14k5kT0NC6mm8925km) 🪙
- [Online POMO manual page](https://docs.google.com/document/d/1rZwnFkl9Pwcee1044AbtH1lA5ErNIbajzn8lCUMq804/edit) 🙋
- [Amazon reviews of POMO](https://www.amazon.com/dp/B08JGSQY8P#customerReviews) 🚚

## How can you help us? 
- We are looking for small surf shops 🏄 who would be interested in having POMO in stock. If you know of any businesses who would like our product reach out!
- Send this page link to your friends, can buy a POMO directly from [here](https://buy.stripe.com/14k5kT0NC6mm8925km) 🔥

## Where can you find POMO?
- Check out a video of [BrianTheCEO](https://www.youtube.com/c/BryanTheCEO) by clicking [here](https://www.instagram.com/p/CI3gI6bHfPl/)
- You can find POMO in San Diego at https://www.pbsurfshop.com/


