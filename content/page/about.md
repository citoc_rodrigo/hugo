---
title: About POMO
subtitle: Where does your portable shower come from?
comments: false
---

POMO is a one person company. But that doesn't mean we don't collaborate with a few different people along the way to bring you a product we hope you love. 

- POMO is manufactured in Mexico by a company called [Swissmex](https://www.swissmex.com/PortalWeb/default.aspx). They make industrial quality agricultural products but we have collaborated together with them to create an innovative product which brings the tough reliability of industrial products to the home. This means less waste, and more bang for your money. 
- We work with a few logistics partners, who help us get POMO to where we see the most demand. Most of the time we work with [Vintage Logistics](https://www.vintage-logistics.com/) but we sometimes work with other companies. 
- You might have noticed that POMO is sold on [Amazon too](amazon.com/dp/B08JGSQY8P). That means that the amazing folks at their distribution centers also help us get POMO to your front door. 

As you can see, your purchase helped out quite a few people along the way. Some large companies and some small, in the end we hope that you got the best deal from POMO. 

### Our history

POMO was started in June 2020, it was a very hard time for us all but we hope that it produced something which helps you enjoy the great outdoors more. 
